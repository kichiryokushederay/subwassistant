var line1 = require('../../data/line1.js');
var line2 = require('../../data/line2.js');
var line3 = require('../../data/line3.js');
var line4 = require('../../data/line4.js');
var line5 = require('../../data/line5.js');
var line6 = require('../../data/line6.js');
var line7 = require('../../data/line7.js');
var line8 = require('../../data/line8.js');
var line9 = require('../../data/line9.js');
var line10 = require('../../data/line10.js');
var line11 = require('../../data/line11.js');
var line12 = require('../../data/line12.js');
var line13 = require('../../data/line13.js');
var line15 = require('../../data/line15.js');
var line16 = require('../../data/line16.js');
var line17 = require('../../data/line17.js');
var line18 = require('../../data/line18.js');
Page({
  data: {
    array: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18],
    index: 0, //默认显示位置
    lineNum: 1,
    table_data: [],
    table_display : false
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  getScheduleData() {
    this.setData({
      lineNum: this.data.array[this.data.index]
    });
    switch (this.data.lineNum) {
      case 1:
        this.setData({
          table_data: line1.line
        });
        break;
      case 2:
        this.setData({
          table_data: line2.line
        });
        break;
      case 3:
        this.setData({
          table_data: line3.line
        });
        break;
      case 4:
        this.setData({
          table_data: line4.line
        });
        break;
      case 5:
        this.setData({
          table_data: line5.line
        });
        break;
      case 6:
        this.setData({
          table_data: line6.line
        });
        break;
      case 7:
        this.setData({
          table_data: line7.line
        });
        break;
      case 8:
        this.setData({
          table_data: line8.line
        });
        break;
      case 9:
        this.setData({
          table_data: line9.line
        });
        break;
      case 10:
        this.setData({
          table_data: line10.line
        });
        break;
      case 11:
        this.setData({
          table_data: line11.line
        });
        break;
      case 12:
        this.setData({
          table_data: line12.line
        });
        break;
      case 13:
        this.setData({
          table_data: line13.line
        });
        break;
      case 15:
        this.setData({
          table_data: line15.line
        });
        break;
      case 16:
        this.setData({
          table_data: line16.line
        });
        break;
      case 17:
        this.setData({
          table_data: line17.line
        });
        break;
      case 18:
        this.setData({
          table_data: line18.line
        });
        break;
      default:
        break;
    }
    this.setData({table_display : true});
  },
  onLoad() {

  },
  onShareAppMessage: function () {

  }
});