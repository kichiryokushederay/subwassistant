// pages/subway/subway.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  usingTencentMap() {
    let plugin = requirePlugin("subway");
    let key = 'ULTBZ-ZTD36-5XPSC-MBM5W-UOMEQ-DDBS5'; //使用在腾讯位置服务申请的key;
    let referer = '地铁助手小程序'; //调用插件的app的名称
    wx.navigateTo({
      url: 'plugin://subway/index?key=' + key + '&referer=' + referer
    });
  },
  usingDIYMap() {
      wx.navigateTo({
        url: '../subway_fig/fig',
      })
  },
  scheduleQuery() {
    wx.navigateTo({
      url: '../schedule/schedule',
    })
  }
})