Page({
  data: {
   cur_city: "北京",
   cur_url : "https://img-blog.csdnimg.cn/4ff4da5f4a4b4d2ab88946a8492def74.png",
   img_url_list : [
      "https://img-blog.csdnimg.cn/4ff4da5f4a4b4d2ab88946a8492def74.png",
      "https://img-blog.csdnimg.cn/237d7cbddcf64138be7e4490b4a65dad.jpg",
      "https://img-blog.csdnimg.cn/eb94bd856e864a608782dca3be5a8b9f.jpg",
     "https://img-blog.csdnimg.cn/a7fd025d028043849c9d655ab235e355.jpg",
     "https://img-blog.csdnimg.cn/4b3335a7a61d43339a79dfe65adc880b.gif",
     "https://img-blog.csdnimg.cn/6732790bea484886827d297f6feacd60.jpg",
     "https://img-blog.csdnimg.cn/9f82fe1435074c808ad1637b218be3b5.png",
     "https://img-blog.csdnimg.cn/adab415037b24ea4ad3ecc6937024b3d.png",
     "https://img-blog.csdnimg.cn/861f94fdd2934825b9bb076d5635b567.png"
   ],
   slist: [
    { id: 1, name: "北京" },
    { id: 1, name: "天津" },
    { id: 1, name: "上海" },
    { id: 1, name: "重庆" },
    { id: 1, name: "广州" },
    { id: 1, name: "深圳" },
    { id: 1, name: "成都" },
    { id: 1, name: "武汉" },
    { id: 1, name: "南京" }
   ],
   isopen: false,
   openimg: "/image/list_unselected.jpg",
   offimg: "/image/list_selected.jpg",
   index : 0
  },
  opens: function (e) {
    switch (e.currentTarget.dataset.item) {
     case "1":
      if (this.data.isopen) {
       this.setData({isopen: false,});
      }
      else {
       this.setData({isopen: true,});
      }
      break;
    }
   },
   onclicks1: function (e) {
    var index = e.currentTarget.dataset.index;
    this.data.index = index;
    this.setData({
     isopen: false,
     cur_city : this.data.slist[index].name,
     cur_url : this.data.img_url_list[index]
    })
   },
   showImage(e) {
     wx.previewImage({
       urls: this.data.img_url_list,
       current: this.data.img_url_list[this.data.index],
       showmenu: true
     })
   },
   onShareAppMessage: function () {

  }
 })