var json = [{'station_name': '虹桥火车站', 'start_time_dir1': '06:00', 'end_time_dir1': '23:00', 'start_time_dir2': '06:20', 'end_time_dir2': '23:10'},
{'station_name': '诸光路', 'start_time_dir1': '06:03', 'end_time_dir1': '23:03', 'start_time_dir2': '06:18', 'end_time_dir2': '23:07'},
{'station_name': '蟠龙路', 'start_time_dir1': '06:05', 'end_time_dir1': '23:05', 'start_time_dir2': '06:15', 'end_time_dir2': '23:05'},
{'station_name': '徐盈路', 'start_time_dir1': '06:08', 'end_time_dir1': '23:08', 'start_time_dir2': '06:12', 'end_time_dir2': '23:02'},
{'station_name': '徐泾北城', 'start_time_dir1': '06:11', 'end_time_dir1': '23:11', 'start_time_dir2': '06:10', 'end_time_dir2': '22:59'},
{'station_name': '嘉松中路', 'start_time_dir1': '06:14', 'end_time_dir1': '23:14', 'start_time_dir2': '06:07', 'end_time_dir2': '22:56'},
{'station_name': '赵巷', 'start_time_dir1': '06:17', 'end_time_dir1': '23:17', 'start_time_dir2': '06:03', 'end_time_dir2': '22:53'},
{'station_name': '汇金路', 'start_time_dir1': '06:21', 'end_time_dir1': '23:21', 'start_time_dir2': '05:59', 'end_time_dir2': '22:49'},
{'station_name': '青浦新城', 'start_time_dir1': '06:24', 'end_time_dir1': '23:24', 'start_time_dir2': '05:56', 'end_time_dir2': '22:46'},
{'station_name': '漕盈路', 'start_time_dir1': '06:27', 'end_time_dir1': '23:28', 'start_time_dir2': '05:53', 'end_time_dir2': '22:42'},
{'station_name': '淀山湖大道', 'start_time_dir1': '06:31', 'end_time_dir1': '23:32', 'start_time_dir2': '05:48', 'end_time_dir2': '22:38'},
{'station_name': '朱家角', 'start_time_dir1': '06:37', 'end_time_dir1': '23:37', 'start_time_dir2': '05:43', 'end_time_dir2': '22:33'},
{'station_name': '东方绿舟', 'start_time_dir1': '06:40', 'end_time_dir1': '23:40', 'start_time_dir2': '05:40', 'end_time_dir2': '22:30'},
];
module.exports = { line : json };