// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    introInfo: [{
      title: "小程序介绍",
      content: "Subwassistant 小程序提供三种地铁出行查询服务：\n一是腾讯官方提供的服务\n二是离线的地铁线路图查询\n三是地铁站点开收班时间查询",
      url: "../../image/swiper_pic_2.png"
    },{
      title: "开发日志8-29",
      content: "完成了下拉菜单选择城市的设计，可以查看静态的地铁图",
      url: null
    },{
      title: "开发日志8-28",
      content: "界面的大致框架有了，能正常使用腾讯提供的地铁线路图和路径规划服务",
      url: null
    },{
      title: "开发日志8-27",
      content: "不再使用接口测试号，正式申请注册了小程序，并在gitee上建立相应仓库",
      url: null
    }],
  },
  // 事件处理函数
  onLoad() {
  },
  onShareAppMessage: function () {

  }
})